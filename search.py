#! /usr/bin/python3.4
import sys
import urllib.request
import urllib.error
import configparser
import smtplib
from email.mime.text import MIMEText

# parsing the configuration file
configFile = configparser.ConfigParser()
configFile.read('search.conf')
conf = configFile['DEFAULT']

# variables
returnCode = -1
handle = conf['handle']

# contacting the url wanted
try:
    rawPage = urllib.request.urlopen("https://twitter.com/" + handle)
    returnCode = rawPage.getcode()

# handling an http error
except urllib.error.HTTPError as err:
    print("HTTP Error", err.code, file=sys.stderr)
    returnCode = err.code

# handling other errors
except:
    print("Error, something bad happened ! Error code: 1", file=sys.stderr)


###########
# Process #
###########
# in the case of code 200, it means the handle is still taken
if returnCode == 200:
    print("Damn ! %s is still taken !" % handle, file=sys.stderr)

# in the case of code 404, it means the handle is not taken
elif returnCode == 404:
    # First, check if the mail has already been sent
    mailSent = False

    try:
        # Case of the mail sent, we do nothing
        f = open('mail.txt', 'r')
        if f.readline() == '1':
            mailSent = True
        f.close()

    # Case of the mail not sent, we will send it right away
    except FileNotFoundError as err:
        f = open('mail.txt', 'w')
        f.write('1')
        f.close()

    except:
        print("Error, something bad happened ! Error code: 2", file=sys.stderr)

    if not mailSent:
        # preparing and sending the mail
        msg = MIMEText('Go get it now !', 'plain')
        msg['Subject'] = 'TwitterCrawler : %s is available on Twitter' % handle
        msg['From'] = conf['mailFrom']
        msg['To'] = conf['mailTo']

        s = smtplib.SMTP(conf['smtpserver'] + ':' + conf['smtpport'])
        s.ehlo()
        s.starttls()
        s.login(conf['mailFrom'], conf['mailpwd'])
        s.send_message(msg)
        s.quit()
