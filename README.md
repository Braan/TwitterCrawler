# TwitterCrawler

**TwitterCrawler** is a small, light and really simple web crawler script written in `Python3` whose only task is to check if the *Twitter* handle you want is available. If it is, it will send you an e-mail (if it has not already).

## Install
Verify that you have Python 3.4 installed, and then just launch `search.py`

## Configuration
For the configuration, you will need to update the `search.conf` file with your information.

It is set by default to use Gmail, but you can probably use another mail service, even if I have only tested Gmail.

:warning: Using Gmail, in order to be able to send a mail through the script you will need to activate an option for less secured app being able to access your account.

### Example of a `search.conf` file
    [DEFAULT]
    # Search
    handle=Twitter
   
    # Mail
    smtpserver=smtp.gmail.com
    smtpport=587
    mailTo=foo@gmail.com
    mailFrom=bar@gmail.com
    mailpwd=azerty


Once you have set up your script, you can automate it.

For example, my configuration is to launch the script every 15 minutes, and erase every morning at 5am the `mail.txt` file generated when the script send you the e-mail.

### Example of a crontab automation
    0 5 * * * rm /usr/local/TwitterCrawler/mail.txt
    */15 * * * * python3 /usr/local/TwitterCrawler/search.py

## Contribute
Contributions are welcome. Feel free to clone, modify or whatever with it.

It is my first script in `Python` so any feedback is helping.

## Links
[Monod](https://monod.lelab.tailordev.fr), the Markdown Editor by [TailorDev](https://twitter.com/_TailorDev) made the creation of this README easier.

## Licence
MIT :copyright: Vincent LAGNEAU